<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/homephp2.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP</title>
</head>
<body>
    <script src="js/about.js"></script>
    <nav>
        <ul>
            <li><a href="about.html">About</a></li>
            <li><a href="home.html">Home</a></li>
            <li><a href="home.php">PHP</a></li>
            <li><a a class="aktif"  href="home2.php">PHP2</a></li>
            <li><a href="home3.php">PHP3</a></li>
            <li class="log1"><input type="button" class="log" value="SIGN IN" onclick="logout()"></li>
        </ul>
    </nav>

    <div class="header">
        <div class="judul">
            <h1>PHP Dasar</h1>
        </div>
        <div class="data1">
          <h2>Percabangan</h2>
          <p>jika nilai anda 75 maka ?</p>
          <?php

            $nilai = 90;

            if ($nilai >= 90) {

              echo "Anda Lulus";

            }elseif ($nilai <= 90){

              echo "Tidak lulus";
            }

          ?>

        </div>

        <div class="data2">
          <h2>PERULANGAN</h2>
          <p>Mencetak angka 1 - 20</p>
          <?php

          for ($i=0; $i <= 20 ; $i++) {

            echo "$i      ";
          }

           ?>
        </div>
    </div>

    <div class="footer">
        <center>
            <p class="copy"> @Copyright 2020 by Arie Hidayattullah</p>
        </center>

    </div>
</body>
</html>
