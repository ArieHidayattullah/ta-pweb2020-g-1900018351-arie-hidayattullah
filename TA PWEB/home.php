<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/homephp.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP</title>
</head>
<body>
    <script src="js/about.js"></script>
    <nav>
        <ul>
            <li><a href="about.html">About</a></li>
            <li><a href="home.html">Home</a></li>
            <li><a a class="aktif" href="home.php">PHP</a></li>
            <li><a href="home2.php">PHP2</a></li>
            <li><a href="home3.php">PHP3</a></li>
            <li class="log1"><input type="button" class="log" value="SIGN IN" onclick="logout()"></li>
        </ul>
    </nav>

    <div class="header">
        <div class="judul">
            <h1>PHP Dasar</h1>
        </div>
        <div class="data1">

          <?php
            $nim = "1900018351";
            $nama = "Arie Hidayattullah";
            $umur = "19";
            $nilai = 88.00;
            $status = TRUE;

            echo "Nama : " .$nama. "<br>";
            echo "NIM  : " .$nim. "<br>";
            print "Umur : " .$umur; print "<br>";
            printf ("Nilai : %.3f <br>", $nilai);
            if( $status ){

              echo "Status : Aktif ";

            }else{

              echo "Status : Tidak Aktif";
            }
          ?>

        </div>
    </div>

    <div class="footer">
        <center>
            <p class="copy"> @Copyright 2020 by Arie Hidayattullah </p>
        </center>

    </div>
</body>
</html>
