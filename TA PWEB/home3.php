<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/homephp3.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP</title>
</head>
<body>
    <script src="js/about.js"></script>
    <nav>
        <ul>
            <li><a href="about.html">About</a></li>
            <li><a href="home.html">Home</a></li>
            <li><a href="home.php">PHP</a></li>
            <li><a href="home2.php">PHP2</a></li>
            <li><a a class="aktif" href="home3.php">PHP3</a></li>
            <li class="log1"><input type="button" class="log" value="SIGN IN" onclick="logout()"></li>
        </ul>
    </nav>

    <div class="header">
        <div class="judul">
            <h1>PHP Dasar</h1>
        </div>
        <form class="input" name="Input" action="" method="post">
          <div class="">

            <label for="">Masukan Nama : </label>
            <input type="text" name="nama" value="">  <br> <br>

          </div>

          <div class="">

            <label for="">Masukan Email : </label>
            <input type="email" name="email" value="">  <br> <br>

          </div>

          <div class="">

            <label for="">Jenis Kelamim <span style="margin-left: 5px">: </span></label>
            <input type="radio" name="Jenis" value="LAKI - LAKI">
            <label for="">LAKI - LAKI</label>
            <input type="radio" name="Jenis" value="PEREMPUAN">
            <label for="">PEREMPUAN</label>
            <br> <br>

          </div>

          <div class="">

            <label for="">Prodi <span style="margin-left: 70px">: </span> </label>
            <select class="" name="Prodi">
              <option value="Teknik Informatika" name="Prodi">Teknik Informatika</option>
              <option value="Teknik Industri" name="Prodi">Teknik Industri</option>
              <option value="Teknik Elektro" name="Prodi">Teknik Elektro</option>
              <option value="Teknik Kimia" name="Prodi">Teknik Kimia</option>
              <option value="Teknik Pangan" name="Prodi">Teknik Pangan</option>
            </select>
             <br> <br>

          </div>

          <div class="">

            <label for="">KRITIK DAN SARAN</label> <br>
            <textarea name="saran" rows="8" cols="40"></textarea>

          </div>

          <div class="">
            <input type="submit" name="input" value="SUBMIT"> <br> <br>
          </div>

        </form>

        <div class="warper">
          <div class="data1dan2">
            <?php
            if (isset($_POST['input'])) {

              $nama = $_POST['nama'];
              $mail = $_POST['email'];
              $jenis = $_POST['Jenis'];
              $prodi = $_POST['Prodi'];
              $kritik = $_POST['saran'];

              echo "Nama Anda     : <b> $nama </b><br>" ;
              echo "E-MAil Anda   : <b> $mail </b> <br>";
              echo "Jenis Kelamim : <b> $jenis </b> <br>";
              echo "Prodi         : <b> $prodi </b> <br>";
              echo "Kritik Dan Saran <br>";
              echo "<b> $kritik </br>";
            }
            ?>

          </div>

        </div>
    </div>
    <div class="footer">
        <center>
            <p class="copy"> @Copyright 2020 by Arie Hidayattullah</p>
        </center>

    </div>
</body>
</html>
